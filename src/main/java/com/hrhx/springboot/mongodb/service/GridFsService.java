package com.hrhx.springboot.mongodb.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
/**
 * 
 * @author duhongming
 *
 */
@Service
public class GridFsService {

	@Autowired

	private GridFsTemplate gridPowerPredictionDBTemplate;

	public void saveFs() {
		try {
			this.getFiles("I:\\DATA1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void findOneFs() {
//		String id = "5602de6e5d8bba0d6f2e45e4";
//		GridFSDBFile gridFsdbFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));
//		List<GridFSDBFile> gridFSDBFiles = gridFsTemplate.find(null);
//		List<GridFSDBFile> gridFsdbFiles = gridFsTemplate.find(new Query(Criteria.where("metadata.user").is("alex")));

	}

	public void deleteFs() {
//		String id = "5702deyu6d8bba0d6f2e45e4";
//		gridFsTemplate.delete(new Query(Criteria.where("_id").is(id)));
	}


	public ArrayList<File> getFiles(String path) throws Exception {
		DBObject metaData = new BasicDBObject();
		metaData.put("company", "北京华软恒信科技发展有限公司");
		metaData.put("user", "杜洪明");
		//目标集合fileList
		ArrayList<File> fileList = new ArrayList<File>();
		File file = new File(path);
		if(file.isDirectory()){
			File []files = file.listFiles();
			for(File fileIndex:files){
				//如果这个文件是目录，则进行递归搜索
				if(fileIndex.isDirectory()){
					getFiles(fileIndex.getPath());
				}else {
					//如果文件是普通文件，则将文件句柄放入集合中
					gridPowerPredictionDBTemplate.store(new FileInputStream(fileIndex.getAbsolutePath()), fileIndex.getName(), "text/csv", metaData);
					System.out.println(fileIndex.getAbsolutePath());
					//fileList.add(fileIndex);
				}
			}
		 }
		return fileList;
	}


	
	public void traverseFolder2(String path) throws FileNotFoundException {
		DBObject metaData = new BasicDBObject();
		metaData.put("company", "北京华软恒信科技发展有限公司");
		metaData.put("user", "duhongming");
        File file = new File(path);
        if (file.exists()) {
            File[] files = file.listFiles();
            if (files.length == 0) {
                System.out.println("文件夹是空的!");
                return;
            } else {
                for (File file2 : files) {
                    if (file2.isDirectory()) {
                        System.out.println("文件夹:" + file2.getAbsolutePath());
                        traverseFolder2(file2.getAbsolutePath());
                    } else {
                        System.out.println("文件:" + file2.getAbsolutePath());
                        InputStream inputStream = new FileInputStream(file2.getAbsolutePath()); 
                        gridPowerPredictionDBTemplate.store(inputStream, file2.getName(), "text/csv", metaData);
                    }
                }
            }
        } else {
            System.out.println("文件不存在!");
        }
    }
	
	

}
