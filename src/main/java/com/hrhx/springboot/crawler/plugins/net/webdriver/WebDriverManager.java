package com.hrhx.springboot.crawler.plugins.net.webdriver;

import org.openqa.selenium.WebDriver;

/**
 * @author duhongming
 * @version 1.0
 * @description selenium WebDriver管理器
 * @date 2019/11/27 10:44
 */
public interface 
WebDriverManager {
    WebDriver getNewInstance(Boolean useProxy);
}